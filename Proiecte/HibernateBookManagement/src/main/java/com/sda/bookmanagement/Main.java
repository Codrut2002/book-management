package com.sda.bookmanagement;

import com.sda.bookmanagement.dao.AuthorDao;
import com.sda.bookmanagement.dao.BookDao;
import com.sda.bookmanagement.dao.ReviewDao;
import com.sda.bookmanagement.model.Author;
import com.sda.bookmanagement.model.Book;
import com.sda.bookmanagement.model.Review;
import org.hibernate.Session;
import org.hibernate.cache.spi.support.AbstractReadWriteAccess;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        // Author

        // Create

        AuthorDao authorDao = new AuthorDao();
//        List<Author> author = authorDao.getAllAuthors();
//        System.out.println("Lista de autori este: " + author);
//        System.out.println();
//        Author auth = new Author("Ion", "Popescu");
//        authorDao.createAuthor(auth);
//        author = authorDao.getAllAuthors();
//        System.out.println("Lista de autori dupa insert este: " + author);
//        System.out.println();

//        Author author1 = new Author("Ion", "Creanga");
//        authorDao.createItemDao(author1);
//        System.out.println("Autorul: " + author1);

        // Update

//        Author au = authorDao.findById(2L);
//        System.out.println("Autorul cautat este: " + au);
//        System.out.println();
//        au.setFirstName("George");
//        authorDao.update(au);

        // Delete

//        Author aut = authorDao.findById(2L);
//        System.out.println("Autorul cautat este: " + aut);
//        System.out.println();
//        authorDao.delete(aut);

        // Book

        // Create

//        BookDao bookDao = new BookDao();
//        Book book = new Book("Amintiri din Copilarie", "fantasy");
//        bookDao.createBook(book);
//        List<Book> books = bookDao.getAllBooks();
//        books = bookDao.getAllBooks();
//        System.out.println("Lista de carti este: " + books);

        // Update

//        Book book2 = bookDao.findById(2L);
//        System.out.println("Cartea cautata este: " + book2);
//        System.out.println();
//        book2.setTitle("Ion");
//        book2.setDescription("comedy");
//        bookDao.update(book2);
//        System.out.println();
//        System.out.println("Cartea dupa update este: " + book2);

        // Delete

//        Book book3 = new Book("Enigma Otiliei", "romance");
//        bookDao.createBook(book3);
//        books.add(book3);
//        books = bookDao.getAllBooks();
//        System.out.println("Am adaugat cartea: " + book3);

//        Book boo = bookDao.findById(3L);
//        System.out.println("Autorul cautat este: " + boo);
//        System.out.println();
//        bookDao.delete(boo);
//        System.out.println("Lista de autori este: " + books);
//        books.remove(book3);
//        System.out.println("Lista de autori este: " + books);

        // Review

        // Create

//        ReviewDao reviewDao = new ReviewDao();
//        Review review = new Review("8/10", "O carte destul de ok.");
//        reviewDao.createReview(review);
//        List<Review> reviews = reviewDao.getAllReviews();
//        System.out.println("Lista de review-uri este: " + reviews);

        // After OneToMany - AuthorBooks

        BookDao bookDao = new BookDao();
//
//        Author author1 = authorDao.findById(1L);
//        Book bookFind = bookDao.findById(1L);
//        System.out.println("Autor: " + author1 + "\nBook: " + bookFind);

        // After OneToMany - BookReviews

//        ReviewDao reviewDao = new ReviewDao();
//
//        Book book1 = bookDao.findById(1L);
//        Review review = new Review(10, "Super", book1);
//        reviewDao.createReview(review);
//        List<Review> reviews = reviewDao.getAllReviews();
//        System.out.println("Cartea: " + book1 + "\nReview: " + review);

        /**
         * Change the author 1
         */

        Author author1 = authorDao.findById(1L);
        author1.setFirstName("Ion");
        author1.setLastName("Creanga");
        authorDao.update(author1);
        System.out.println("Numele autorului dupa update este: " + author1);

        Book book1 = bookDao.findById(1L);
        System.out.println("Cartea: " + book1);


    }
}
