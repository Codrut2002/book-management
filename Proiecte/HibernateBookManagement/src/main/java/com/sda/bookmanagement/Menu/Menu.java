package com.sda.bookmanagement.Menu;

import com.sda.bookmanagement.dao.AuthorDao;
import com.sda.bookmanagement.dao.BookDao;
import com.sda.bookmanagement.dao.ReviewDao;
import com.sda.bookmanagement.model.Author;
import com.sda.bookmanagement.model.Book;
import com.sda.bookmanagement.model.Review;

import java.util.List;
import java.util.Scanner;

public class Menu {
    private AuthorDao authorDao = new AuthorDao();
    private ReviewDao reviewDao = new ReviewDao();
    private BookDao bookDao = new BookDao();
    private Scanner in = new Scanner(System.in);

    public void displayMenu(){
        System.out.println("0. Exit\n" +
                "1. Create Book\n" +
                "2. Delete Book\n" +
                "3. Update Book\n" +
                "4. Show Books\n" +
                "5. Create Author\n" +
                "6. Delete Author\n" +
                "7. Update Author\n" +
                "8. Show Authors\n" +
                "9. Add Review\n" +
                "10. Show Reviews");
    }
    public int chooseOption(){
        System.out.println("Choose an option: ");
        int option = in.nextInt();
        switch (option){
            case 0:
                System.out.println("Program is closing...");
                return option;
            case 1:
                // Create a book
                createBook();
                break;
            case 2:
                // Delete a book
                deleteBook();
                break;
            case 3:
                // Update a book
                updateBook();
                break;
            case 4:
                // Show a books
                showBooks();
                break;
            case 5:
                // Create a author
                createAuthor();
                break;
            case 6:
                // Delete a author
                deleteAuthor();
                break;
            case 7:
                // Update a author

                break;
            case 8:
                // Show authors
                showAuthors();
                break;
            case 9:
                // Add review
                addReview();
                break;
            case 10:
                // Show reviews
                showReviews();
                break;
            default:
                System.out.println("Option is invalid");
                this.chooseOption();
        }
        return 1;
    }
    private void showBooks(){
        System.out.println("These are the books from database:");
        List<Book> bookList = bookDao.getAllBooks();
        System.out.println(bookList);
    }

    private void showAuthors() {
        System.out.println("These are the authors from database:");
        List<Author> authorList = authorDao.getAllAuthors();
        System.out.println(authorList);
    }

    public void showReviews() {
        System.out.println("These are the reviews from database:");
        List<Review> reviewList = reviewDao.getAllReviews();
        System.out.println(reviewList);
    }

    private void createBook(){
        System.out.println("You chose to create a book");
        System.out.println("Title: ");
        in.nextLine();
        String title = in.nextLine();
        System.out.println("Description: ");
        String description = in.nextLine();
        System.out.println("Choose an author");
        showAuthors();
        System.out.println("Author id:");
        int authorId = in.nextInt();
        Author auth = authorDao.findById(authorId);
        Book book = new Book(title, description, auth);
        bookDao.createBook(book);
        System.out.println("The book was created successfully");
    }

    private void deleteBook() {
        System.out.println("You chose to delete a book");
        showBooks();
        System.out.println("Which book you want to delete: ");
        int bookId = in.nextInt();
        Book book1 = bookDao.findById(bookId);
        bookDao.delete(book1);
        System.out.println("The book was deleted successfully");
    }

    private void createAuthor() {
        System.out.println("You chose to create a author");
        System.out.println("Firstname: ");
        in.nextLine();
        String firstname = in.nextLine();
        System.out.println("Lastname: ");
        String lastname = in.nextLine();
        Author author = new Author(firstname, lastname);
        authorDao.createAuthor(author);
        System.out.println("The author was created successfully");
    }

    private void deleteAuthor() {
        System.out.println("You chose to delete a author");
        showAuthors();
        System.out.println("Which author you want to delete: ");
        int authorId = in.nextInt();
        Author author1 = authorDao.findById(authorId);
        authorDao.delete(author1);
        System.out.println("The author was deleted successfully");
    }

    private void addReview() {
        System.out.println("You chose to add a review");
        System.out.println("Score: ");
        in.nextInt();
        int score = in.nextInt();
        System.out.println("Comment: ");
        in.nextLine();
        String comment = in.nextLine();
        System.out.println("Choose a book to give this review");
        showBooks();
        System.out.println("Book id: ");
        int bookId = in.nextInt();
        Book book = bookDao.findById(bookId);
        Review review = new Review(score, comment, book);
        reviewDao.createReview(review);
        System.out.println("The review was added successfully");
    }

    private void updateBook() {
        System.out.println("You chose to update a book");
        System.out.println("Choose a book to update");
        showBooks();
        System.out.println("Book Id: ");
        int bookId = in.nextInt();
        Book book = bookDao.findById(bookId);
        System.out.println("What do you want to update?");
        System.out.println();
        System.out.println("0. Nothing\n" +
                "1. The Title\n" +
                "2. The Description");
        int option = in.nextInt();
        switch (option) {
            case 0:
                System.out.println("Program is closing...");
                break;
            case 1:
                // The title
                System.out.println("Choose a new title: ");
                in.nextLine();
                String title = in.nextLine();
                book.setTitle(title);
                bookDao.update(book);
                System.out.println("You have updated the title");
                break;
            case 2:
                // The description
                System.out.println("Choose a description: ");
                in.nextLine();
                String description = in.nextLine();
                book.setDescription(description);
                bookDao.update(book);
                System.out.println("You have updated the description");
                break;
            default:
                System.out.println("Option is invalid..");

    }


//        System.out.println("Do you want to change the title?");
//        System.out.println("Y / N: ");
//        in.nextLine();
//        String option = in.nextLine();
//        if (option.equals("Y") || option.equals("y")) {
//            System.out.println("Choose a new title: ");
//            String title = in.nextLine();
//            book.setTitle(title);
//            bookDao.update(book);
//            System.out.println("You have updated the title");
//        }
//        System.out.println("Do you want to change the description?");
//        System.out.println("Y / N: ");
//        in.nextLine();
//        String option2 = in.nextLine();
//        if (option2.equals("Y") || option2.equals("y")) {
//            System.out.println("Choose a description: ");
//            String description = in.nextLine();
//            book.setDescription(description);
//            bookDao.update(book);
//            System.out.println("You have updated the description");
//        }
    }
}