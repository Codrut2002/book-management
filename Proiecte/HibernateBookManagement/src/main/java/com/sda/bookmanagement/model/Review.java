package com.sda.bookmanagement.model;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table
public class Review {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;

    @Column(name = "score")
    private int score;

    @Column(name = "comment")
    private String comment;

    @ManyToOne
    @JoinColumn(name = "book_id")
    private Book book;

    public Review() {

    }

    public Review(int score, String comment, Book book) {
        this.score = score;
        this.comment = comment;
        this.book = book;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
//        if (score < 1 || score > 10) {
//            throw new RuntimeException("Scorul nu este intre 1 si 10");
//        }
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Override
    public String toString() {
        return "Review{" +
                "id=" + id +
                ", score=" + score +
                ", comment='" + comment + '\'' +
                ", book=" + book +
                "}\n";
    }
}
