package com.sda.bookmanagement.Menu;

import com.sda.bookmanagement.HibernateUtils;
import org.hibernate.Session;

public class Main {
    public static void main(String[] args) {
        Session s = HibernateUtils.getSessionFactory().openSession();
        s.close();
        Menu menu = new Menu();
        int option = 1;
        while(option != 0) {
            menu.displayMenu();
            option = menu.chooseOption();

        }
    }
}
